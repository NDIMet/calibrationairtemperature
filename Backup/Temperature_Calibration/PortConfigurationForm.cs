﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Temperature_Calibration
{
    public partial class PortConfigurationForm : Form
    {
        public PortConfigurationForm()
        {
            InitializeComponent();
        }

        public Form1 dataPasser;
        public Setup newSetup;

        private void PortConfigurationForm_Load(object sender, EventArgs e)
        {
            this.dataPasser.currentSetup.getSetupFromFile(@"C:\Temperature Calibration\Setup.cfg");
            this.newSetup = dataPasser.currentSetup;
            //Set Root Directory
            textBoxRootDirectory.Text = dataPasser.currentSetup.rootDirectory;
            //Set Reference Port Information
            comboBoxReferencePort.DataSource = System.IO.Ports.SerialPort.GetPortNames();
            comboBoxReferencePort.SelectedItem = dataPasser.currentSetup.referenceMuxPort;
            //Set Reference Steinhart-Hart Coefficients
            textBoxRefA0.Text = dataPasser.currentSetup.referenceA0.ToString("+0.0000000000e+0;-0.0000000000e+0");
            textBoxRefA1.Text = dataPasser.currentSetup.referenceA1.ToString("+0.0000000000e+0;-0.0000000000e+0");
            textBoxRefA2.Text = dataPasser.currentSetup.referenceA2.ToString("+0.0000000000e+0;-0.0000000000e+0");
            textBoxRefA3.Text = dataPasser.currentSetup.referenceA3.ToString("+0.0000000000e+0;-0.0000000000e+0");
            textBoxRefA4.Text = dataPasser.currentSetup.referenceA4.ToString("+0.0000000000e+0;-0.0000000000e+0");
            textBoxRefA5.Text = dataPasser.currentSetup.referenceA5.ToString("+0.0000000000e+0;-0.0000000000e+0");
            //Set Chamber Information
            comboBoxChamberPort.DataSource = System.IO.Ports.SerialPort.GetPortNames();
            comboBoxChamberPort.SelectedItem = dataPasser.currentSetup.temperatureChamberCOMPort;
            //Set Relay Board Information
            comboBoxRelayBoardPort.DataSource = System.IO.Ports.SerialPort.GetPortNames();
            comboBoxRelayBoardPort.SelectedItem = dataPasser.currentSetup.relayBoardCOMPort;
            //Set Probe Multiplexer Information
            textBoxProbeIP.Text = dataPasser.currentSetup.probeMuxIPAddress;
            textBoxPort.Text = dataPasser.currentSetup.probeMuxPort.ToString();
            textBoxCards.Text = dataPasser.currentSetup.numberOfCards.ToString();
        }

        private void buttonRootDirectory_Click(object sender, EventArgs e)
        {
            folderBrowserDialogRoot.SelectedPath = dataPasser.currentSetup.rootDirectory;

            if (folderBrowserDialogRoot.ShowDialog() == DialogResult.OK) 
            {
                textBoxRootDirectory.Text = folderBrowserDialogRoot.SelectedPath;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (checkUserData())
            {
                this.dataPasser.currentSetup.rootDirectory = this.newSetup.rootDirectory;
                this.dataPasser.currentSetup.referenceMuxPort = this.newSetup.referenceMuxPort;
                this.dataPasser.currentSetup.referenceA0 = this.newSetup.referenceA0;
                this.dataPasser.currentSetup.referenceA1 = this.newSetup.referenceA1;
                this.dataPasser.currentSetup.referenceA2 = this.newSetup.referenceA2;
                this.dataPasser.currentSetup.referenceA3 = this.newSetup.referenceA3;
                this.dataPasser.currentSetup.referenceA4 = this.newSetup.referenceA4;
                this.dataPasser.currentSetup.referenceA5 = this.newSetup.referenceA5;
                this.dataPasser.currentSetup.temperatureChamberCOMPort = this.newSetup.temperatureChamberCOMPort;
                this.dataPasser.currentSetup.relayBoardCOMPort = this.newSetup.relayBoardCOMPort;
                this.dataPasser.currentSetup.probeMuxIPAddress = this.newSetup.probeMuxIPAddress;
                this.dataPasser.currentSetup.probeMuxPort = this.newSetup.probeMuxPort;
                this.dataPasser.currentSetup.numberOfCards = this.newSetup.numberOfCards;

                this.dataPasser.currentSetup.setFileFromSetup();
                this.Close();
            }

        }
        private bool checkUserData() 
        {
            //Check the root directory
            if (!System.IO.Directory.Exists(textBoxRootDirectory.Text))
            {
                MessageBox.Show("The root directory does not exist. Enter a valid root directory.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRootDirectory.Select();
                return false;
            }
            else 
            {
                this.newSetup.rootDirectory = textBoxRootDirectory.Text;
            }
            //Check to make sure COM ports are not the same
            if (comboBoxChamberPort.SelectedItem.Equals(comboBoxReferencePort.SelectedItem)) 
            {
                MessageBox.Show("The chamber and reference multiplexer cannot have the same COM port. Change one of the COM ports.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                comboBoxReferencePort.Select();
                return false;
            }
            //Make sure the COM ports exist
            string[] COMPortNames = System.IO.Ports.SerialPort.GetPortNames();
            if (Array.IndexOf(COMPortNames, comboBoxReferencePort.SelectedItem) == -1) 
            {
                MessageBox.Show("Invalid reference multiplexer COM port selected. Select a valid port.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                comboBoxReferencePort.Select();
                return false;
            }
            if (Array.IndexOf(COMPortNames, comboBoxChamberPort.SelectedItem) == -1) 
            {
                MessageBox.Show("Invalid temperature chamber COM port selected. Select a valid port.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                comboBoxReferencePort.Select();
                return false;
            }
            //COM ports are OK to add now
            this.newSetup.referenceMuxPort = (string)comboBoxReferencePort.SelectedItem;
            this.newSetup.temperatureChamberCOMPort = (string)comboBoxChamberPort.SelectedItem;
            this.newSetup.relayBoardCOMPort = (string)comboBoxRelayBoardPort.SelectedItem;

            //Check coefficients
            double currentCoefficient;
            try
            {
                currentCoefficient = double.Parse(textBoxRefA0.Text);
                this.newSetup.referenceA0 = currentCoefficient;
            }
            catch 
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA0.Select();
                return false;
            }
            try
            {
                currentCoefficient = double.Parse(textBoxRefA1.Text);
                this.newSetup.referenceA1 = currentCoefficient;

            }
            catch
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA1.Select();
                return false;
            }
            try
            {
                currentCoefficient = double.Parse(textBoxRefA2.Text);
                this.newSetup.referenceA2 = currentCoefficient;
            }
            catch
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA2.Select();
                return false;
            }
            try
            {
                currentCoefficient = double.Parse(textBoxRefA3.Text);
                this.newSetup.referenceA3 = currentCoefficient;
            }
            catch
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA3.Select();
                return false;
            }
            try
            {
                currentCoefficient = double.Parse(textBoxRefA4.Text); 
                this.newSetup.referenceA4 = currentCoefficient;
            }
            catch
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA4.Select();
                return false;
            }
            try
            {
                currentCoefficient = double.Parse(textBoxRefA5.Text);
                this.newSetup.referenceA5 = currentCoefficient;
            }
            catch
            {
                MessageBox.Show("Invalid data type entered for the coefficient. Enter a decimal number in the format '+#.#########e+#'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxRefA5.Select();
                return false;
            }

            //Check IP setting for probe multiplexer
            string[] splitLine = textBoxProbeIP.Text.Split('.');
            bool goodIPaddress = true;
            if (splitLine.Length != 4) 
            {
                goodIPaddress = false;
            }
            for (int i = 0; i < splitLine.Length; i++) 
            {
                try
                {
                    int number = int.Parse(splitLine[i]);
                    if (number > 255 || number < 0) 
                    {
                        goodIPaddress = false;
                    }
                }
                catch 
                {
                    goodIPaddress = false;
                }
            }
            if (!goodIPaddress) 
            {
                MessageBox.Show("Invalid IP address entered for the probe multiplexer. Enter an IP address in the format #.#.#.# where # is between 0 and 255", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxProbeIP.Select();
                return false;
            }
            this.newSetup.probeMuxIPAddress = textBoxProbeIP.Text;

            //Check the port
            try
            {
                this.newSetup.probeMuxPort = int.Parse(textBoxPort.Text);
            }
            catch 
            {
                MessageBox.Show("Invalid port entered for the probe multiplexer. Enter an IP address in the format #.#.#.# where # is between 0 and 255", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxPort.Select();
                return false;
            }
            //Check the cards
            int newNumberOfCards;
            try
            {
                newNumberOfCards = int.Parse(textBoxCards.Text);
                if (newNumberOfCards < 1 || newNumberOfCards > 8) 
                {
                    throw new System.InvalidOperationException();
                }
            }
            catch 
            {
                MessageBox.Show("Invalid number of cards entered. Enter an integer between 1 and 8.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxCards.Select();
                return false;
            }
            
            this.newSetup.numberOfCards = newNumberOfCards;

            return true;
        }
    }
}
