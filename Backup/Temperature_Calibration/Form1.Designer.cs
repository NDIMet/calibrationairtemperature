﻿namespace Temperature_Calibration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateCoefficientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzedCalibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calibrationConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxChamber = new System.Windows.Forms.GroupBox();
            this.labelCurrentUnits = new System.Windows.Forms.Label();
            this.labelSetpointUnits = new System.Windows.Forms.Label();
            this.labelTemp = new System.Windows.Forms.Label();
            this.labelChamberSetpoint = new System.Windows.Forms.Label();
            this.textBoxChamberTemperature = new System.Windows.Forms.TextBox();
            this.textBoxChamberSetpoint = new System.Windows.Forms.TextBox();
            this.groupBoxStability = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRefStability = new System.Windows.Forms.TextBox();
            this.textBoxChamberStability = new System.Windows.Forms.TextBox();
            this.groupBoxReference = new System.Windows.Forms.GroupBox();
            this.labelRefTempUnits = new System.Windows.Forms.Label();
            this.labelRefResistanceUnits = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxRefTemp = new System.Windows.Forms.TextBox();
            this.textBoxRefResistance = new System.Windows.Forms.TextBox();
            this.groupBoxProbes = new System.Windows.Forms.GroupBox();
            this.textBoxNumProbes = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.groupBoxChamber.SuspendLayout();
            this.groupBoxStability.SuspendLayout();
            this.groupBoxReference.SuspendLayout();
            this.groupBoxProbes.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxStatus.Location = new System.Drawing.Point(-1, 246);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(376, 20);
            this.textBoxStatus.TabIndex = 0;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConnect.Location = new System.Drawing.Point(288, 217);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 1;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.Enabled = false;
            this.buttonStart.Location = new System.Drawing.Point(207, 217);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.setupToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(375, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculateCoefficientsToolStripMenuItem,
            this.analyzedCalibrationToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // calculateCoefficientsToolStripMenuItem
            // 
            this.calculateCoefficientsToolStripMenuItem.Name = "calculateCoefficientsToolStripMenuItem";
            this.calculateCoefficientsToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.calculateCoefficientsToolStripMenuItem.Text = "Calculate Coefficients";
            this.calculateCoefficientsToolStripMenuItem.Click += new System.EventHandler(this.calculateCoefficientsToolStripMenuItem_Click);
            // 
            // analyzedCalibrationToolStripMenuItem
            // 
            this.analyzedCalibrationToolStripMenuItem.Name = "analyzedCalibrationToolStripMenuItem";
            this.analyzedCalibrationToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.analyzedCalibrationToolStripMenuItem.Text = "Analyze Calibration";
            this.analyzedCalibrationToolStripMenuItem.Click += new System.EventHandler(this.analyzedCalibrationToolStripMenuItem_Click);
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.portConfigurationToolStripMenuItem,
            this.calibrationConfigurationToolStripMenuItem});
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.setupToolStripMenuItem.Text = "Setup";
            // 
            // portConfigurationToolStripMenuItem
            // 
            this.portConfigurationToolStripMenuItem.Name = "portConfigurationToolStripMenuItem";
            this.portConfigurationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.portConfigurationToolStripMenuItem.Text = "Port Configuration";
            this.portConfigurationToolStripMenuItem.Click += new System.EventHandler(this.portConfigurationToolStripMenuItem_Click);
            // 
            // calibrationConfigurationToolStripMenuItem
            // 
            this.calibrationConfigurationToolStripMenuItem.Name = "calibrationConfigurationToolStripMenuItem";
            this.calibrationConfigurationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.calibrationConfigurationToolStripMenuItem.Text = "Calibration Configuration";
            // 
            // groupBoxChamber
            // 
            this.groupBoxChamber.Controls.Add(this.labelCurrentUnits);
            this.groupBoxChamber.Controls.Add(this.labelSetpointUnits);
            this.groupBoxChamber.Controls.Add(this.labelTemp);
            this.groupBoxChamber.Controls.Add(this.labelChamberSetpoint);
            this.groupBoxChamber.Controls.Add(this.textBoxChamberTemperature);
            this.groupBoxChamber.Controls.Add(this.textBoxChamberSetpoint);
            this.groupBoxChamber.Location = new System.Drawing.Point(12, 27);
            this.groupBoxChamber.Name = "groupBoxChamber";
            this.groupBoxChamber.Size = new System.Drawing.Size(171, 80);
            this.groupBoxChamber.TabIndex = 4;
            this.groupBoxChamber.TabStop = false;
            this.groupBoxChamber.Text = "Chamber";
            // 
            // labelCurrentUnits
            // 
            this.labelCurrentUnits.AutoSize = true;
            this.labelCurrentUnits.Location = new System.Drawing.Point(135, 48);
            this.labelCurrentUnits.Name = "labelCurrentUnits";
            this.labelCurrentUnits.Size = new System.Drawing.Size(20, 13);
            this.labelCurrentUnits.TabIndex = 5;
            this.labelCurrentUnits.Text = "[C]";
            // 
            // labelSetpointUnits
            // 
            this.labelSetpointUnits.AutoSize = true;
            this.labelSetpointUnits.Location = new System.Drawing.Point(135, 22);
            this.labelSetpointUnits.Name = "labelSetpointUnits";
            this.labelSetpointUnits.Size = new System.Drawing.Size(20, 13);
            this.labelSetpointUnits.TabIndex = 4;
            this.labelSetpointUnits.Text = "[C]";
            // 
            // labelTemp
            // 
            this.labelTemp.AutoSize = true;
            this.labelTemp.Location = new System.Drawing.Point(7, 48);
            this.labelTemp.Name = "labelTemp";
            this.labelTemp.Size = new System.Drawing.Size(41, 13);
            this.labelTemp.TabIndex = 3;
            this.labelTemp.Text = "Current";
            // 
            // labelChamberSetpoint
            // 
            this.labelChamberSetpoint.AutoSize = true;
            this.labelChamberSetpoint.Location = new System.Drawing.Point(7, 22);
            this.labelChamberSetpoint.Name = "labelChamberSetpoint";
            this.labelChamberSetpoint.Size = new System.Drawing.Size(46, 13);
            this.labelChamberSetpoint.TabIndex = 2;
            this.labelChamberSetpoint.Text = "Setpoint";
            // 
            // textBoxChamberTemperature
            // 
            this.textBoxChamberTemperature.Location = new System.Drawing.Point(80, 45);
            this.textBoxChamberTemperature.Name = "textBoxChamberTemperature";
            this.textBoxChamberTemperature.ReadOnly = true;
            this.textBoxChamberTemperature.Size = new System.Drawing.Size(49, 20);
            this.textBoxChamberTemperature.TabIndex = 1;
            // 
            // textBoxChamberSetpoint
            // 
            this.textBoxChamberSetpoint.Location = new System.Drawing.Point(80, 19);
            this.textBoxChamberSetpoint.Name = "textBoxChamberSetpoint";
            this.textBoxChamberSetpoint.ReadOnly = true;
            this.textBoxChamberSetpoint.Size = new System.Drawing.Size(49, 20);
            this.textBoxChamberSetpoint.TabIndex = 0;
            // 
            // groupBoxStability
            // 
            this.groupBoxStability.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxStability.Controls.Add(this.label1);
            this.groupBoxStability.Controls.Add(this.label2);
            this.groupBoxStability.Controls.Add(this.label3);
            this.groupBoxStability.Controls.Add(this.label4);
            this.groupBoxStability.Controls.Add(this.textBoxRefStability);
            this.groupBoxStability.Controls.Add(this.textBoxChamberStability);
            this.groupBoxStability.Location = new System.Drawing.Point(207, 27);
            this.groupBoxStability.Name = "groupBoxStability";
            this.groupBoxStability.Size = new System.Drawing.Size(156, 80);
            this.groupBoxStability.TabIndex = 5;
            this.groupBoxStability.TabStop = false;
            this.groupBoxStability.Text = "Stability";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(125, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "[C]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(125, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "[C]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Reference";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Chamber";
            // 
            // textBoxRefStability
            // 
            this.textBoxRefStability.Location = new System.Drawing.Point(70, 45);
            this.textBoxRefStability.Name = "textBoxRefStability";
            this.textBoxRefStability.ReadOnly = true;
            this.textBoxRefStability.Size = new System.Drawing.Size(49, 20);
            this.textBoxRefStability.TabIndex = 1;
            // 
            // textBoxChamberStability
            // 
            this.textBoxChamberStability.Location = new System.Drawing.Point(70, 19);
            this.textBoxChamberStability.Name = "textBoxChamberStability";
            this.textBoxChamberStability.ReadOnly = true;
            this.textBoxChamberStability.Size = new System.Drawing.Size(49, 20);
            this.textBoxChamberStability.TabIndex = 0;
            // 
            // groupBoxReference
            // 
            this.groupBoxReference.Controls.Add(this.labelRefTempUnits);
            this.groupBoxReference.Controls.Add(this.labelRefResistanceUnits);
            this.groupBoxReference.Controls.Add(this.label7);
            this.groupBoxReference.Controls.Add(this.label8);
            this.groupBoxReference.Controls.Add(this.textBoxRefTemp);
            this.groupBoxReference.Controls.Add(this.textBoxRefResistance);
            this.groupBoxReference.Location = new System.Drawing.Point(12, 113);
            this.groupBoxReference.Name = "groupBoxReference";
            this.groupBoxReference.Size = new System.Drawing.Size(171, 80);
            this.groupBoxReference.TabIndex = 6;
            this.groupBoxReference.TabStop = false;
            this.groupBoxReference.Text = "Reference";
            // 
            // labelRefTempUnits
            // 
            this.labelRefTempUnits.AutoSize = true;
            this.labelRefTempUnits.Location = new System.Drawing.Point(135, 48);
            this.labelRefTempUnits.Name = "labelRefTempUnits";
            this.labelRefTempUnits.Size = new System.Drawing.Size(20, 13);
            this.labelRefTempUnits.TabIndex = 5;
            this.labelRefTempUnits.Text = "[C]";
            // 
            // labelRefResistanceUnits
            // 
            this.labelRefResistanceUnits.AutoSize = true;
            this.labelRefResistanceUnits.Location = new System.Drawing.Point(135, 22);
            this.labelRefResistanceUnits.Name = "labelRefResistanceUnits";
            this.labelRefResistanceUnits.Size = new System.Drawing.Size(0, 13);
            this.labelRefResistanceUnits.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Temperature";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Resistance";
            // 
            // textBoxRefTemp
            // 
            this.textBoxRefTemp.Location = new System.Drawing.Point(80, 45);
            this.textBoxRefTemp.Name = "textBoxRefTemp";
            this.textBoxRefTemp.ReadOnly = true;
            this.textBoxRefTemp.Size = new System.Drawing.Size(49, 20);
            this.textBoxRefTemp.TabIndex = 1;
            // 
            // textBoxRefResistance
            // 
            this.textBoxRefResistance.Location = new System.Drawing.Point(80, 19);
            this.textBoxRefResistance.Name = "textBoxRefResistance";
            this.textBoxRefResistance.ReadOnly = true;
            this.textBoxRefResistance.Size = new System.Drawing.Size(49, 20);
            this.textBoxRefResistance.TabIndex = 0;
            // 
            // groupBoxProbes
            // 
            this.groupBoxProbes.Controls.Add(this.textBoxNumProbes);
            this.groupBoxProbes.Location = new System.Drawing.Point(12, 199);
            this.groupBoxProbes.Name = "groupBoxProbes";
            this.groupBoxProbes.Size = new System.Drawing.Size(171, 41);
            this.groupBoxProbes.TabIndex = 7;
            this.groupBoxProbes.TabStop = false;
            this.groupBoxProbes.Text = "Num of Probes";
            // 
            // textBoxNumProbes
            // 
            this.textBoxNumProbes.Location = new System.Drawing.Point(80, 15);
            this.textBoxNumProbes.Name = "textBoxNumProbes";
            this.textBoxNumProbes.ReadOnly = true;
            this.textBoxNumProbes.Size = new System.Drawing.Size(49, 20);
            this.textBoxNumProbes.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 266);
            this.Controls.Add(this.groupBoxProbes);
            this.Controls.Add(this.groupBoxReference);
            this.Controls.Add(this.groupBoxStability);
            this.Controls.Add(this.groupBoxChamber);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Temperature Calibration 1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxChamber.ResumeLayout(false);
            this.groupBoxChamber.PerformLayout();
            this.groupBoxStability.ResumeLayout(false);
            this.groupBoxStability.PerformLayout();
            this.groupBoxReference.ResumeLayout(false);
            this.groupBoxReference.PerformLayout();
            this.groupBoxProbes.ResumeLayout(false);
            this.groupBoxProbes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portConfigurationToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxChamber;
        private System.Windows.Forms.TextBox textBoxChamberTemperature;
        private System.Windows.Forms.TextBox textBoxChamberSetpoint;
        private System.Windows.Forms.Label labelCurrentUnits;
        private System.Windows.Forms.Label labelSetpointUnits;
        private System.Windows.Forms.Label labelTemp;
        private System.Windows.Forms.Label labelChamberSetpoint;
        private System.Windows.Forms.GroupBox groupBoxStability;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRefStability;
        private System.Windows.Forms.TextBox textBoxChamberStability;
        private System.Windows.Forms.GroupBox groupBoxReference;
        private System.Windows.Forms.Label labelRefTempUnits;
        private System.Windows.Forms.Label labelRefResistanceUnits;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxRefTemp;
        private System.Windows.Forms.TextBox textBoxRefResistance;
        private System.Windows.Forms.GroupBox groupBoxProbes;
        private System.Windows.Forms.TextBox textBoxNumProbes;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateCoefficientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analyzedCalibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calibrationConfigurationToolStripMenuItem;
    }
}

