﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestEquipment;
using System.Net.Sockets;
using System.Net;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace Temperature_Calibration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Global Variables
        public bool loggingChamberData = false;
        public bool loggingReference = true;
        public bool readyForCalibration = false;
        public string[] validProbes;
        public double currentTemperatureSetpoint = 0;
        public double currentTemperature;

        //Global Objects
        public Setup currentSetup;
        public Watlow temperatureChamber;
        public Agilent34970A referenceMux;
        public Agilent34980A probeMux;
        public BBModule relayBoard;
        public DateTime timeDatum;
        public string[,] channelMap;
        public UInt64 firstProbeID;
        public int numberOfPanels;
        public System.IO.TextWriter[] probeRawFiles;
        public System.IO.TextWriter referenceFile;
        public string[] probeRawFileNames;
        public string referenceFileName;
        public string dateTimeFolder;

        public System.Threading.AutoResetEvent childFormClose;
        public System.Threading.AutoResetEvent environmentStable;
        public System.Threading.AutoResetEvent referenceDataIn;

        //Background Workers
        public BackgroundWorker InitializeEquipment;
        public BackgroundWorker RunCalibration;
        public System.Threading.Thread chamberMonitorThread;

        //Delegates
        delegate void UpdateStatus(string message);
        delegate void UpdateChamber(string setpoint, string temperature);
        delegate void UpdateChamberStability(string stability);
        delegate void UpdateReferenceData(string resistance, string temperature,string stability);
        delegate void ButtonConfiguration(bool initializeButton, bool calibrateButton);

        static readonly object _chamberLock = new object();
        static readonly object _referenceLock = new object();

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            this.InitializeEquipment = new BackgroundWorker();
            this.InitializeEquipment.DoWork += new DoWorkEventHandler(InitializeEquipment_DoWork);
            this.InitializeEquipment.RunWorkerAsync();
        }

        void InitializeEquipment_DoWork(object sender, DoWorkEventArgs e)
        {
            //Disable both buttons
            this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { false, false });

            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Connecting to temperature chamber..." });
            
            if (connectToChamber())
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Temperature Chamber Online!" });
            }
            else
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Error connecting to the chamber. Check COM port settings and click 'Connect'." });
                MessageBox.Show("Error connecting to the chamber. Check COM port settings and click 'Connect'.","Initialization Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                this.temperatureChamber.chamber.Close();
                InitializeEquipment.Dispose();
                //Turn "Connect" button back on
                this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { true, false });
                return;
            }

            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Connecting to 4-Wire Multiplexer..." });

            if (connectToReferenceMux())
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "4-Wire Multiplexer Online!" });
                System.Threading.Thread reference4WireMonitor = new System.Threading.Thread(this.referenceMonitor);
                reference4WireMonitor.Start();
            }
            else 
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Error connecting to reference multimeter." });
                MessageBox.Show("Error connecting to the reference multimeter. Check COM port settings and click 'Connect'.", "Initialization Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.referenceMux.SerialPort.Dispose();
                InitializeEquipment.Dispose();
                //Turn "Connect" button back on
                this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { true, false });

                return;
            }
            
            System.Threading.Thread.Sleep(500);

            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Connecting to the Relay Board..." });

            if (!connectToRelayBoard()) 
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Error connecting to relay board." });
                MessageBox.Show("Error connecting to the relay board. Check COM port settings and click 'Connect'.");
                this.relayBoard.ComPort.Dispose();
                InitializeEquipment.Dispose();
                return;
            }
            
            if (connectToProbeMux())
            {
                //Intentionally left blank
            }
            else 
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Error connecting to reference multimeter." });
                MessageBox.Show("Error connecting to the probe multimeter. Check COM port settings and click 'Connect'.");
                InitializeEquipment.Dispose();
                return;
            }

            System.Threading.Thread.Sleep(500);

            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Connecting to probes..." });
            this.channelMap = verifyProbes();

            System.Threading.Thread.Sleep(500);

            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Initializing Text Files..." });
            initializeTextFiles();
            if (!this.readyForCalibration) 
            {
                this.InitializeEquipment.Dispose();
                //Turn on the "Connect" button and turn off the "Start" button
                this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { true, false });

                return;
            }
            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Text files complete. Ready to begin calibration." });

            //Turn off the "Connect" button and turn on the "Start" button
            this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { false, true });
        }

        void RunCalibration_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Invoke(new ButtonConfiguration(this.UpdateButtonsEnabled), new object[] { false,false });
            this.timeDatum = DateTime.Now;
            
            //Turn fans on
            this.relayBoard.setOutput(0, true);

            //Go to temperature setpoints and log the data
            foreach (double setpoint in this.currentSetup.temperatureSetpoints)
            {
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Attempting temperature setpoint "+setpoint.ToString("0.0")+" C..." });
                goToTemperatureSetpoint(setpoint);
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Beginning to log calibration data.." });
                System.Threading.Thread.Sleep(1000);
     
                logCalibrationData(this.currentSetup.numberOfDataPoints);
            }

            goToTemperatureSetpoint(25.0);

            //Turn fans off
            this.relayBoard.setOutput(0, false);
            //Turn heaters off
            this.relayBoard.setOutput(1, false);

            closeLogFiles();
            
            //Calculate the coefficients
            double[] coefficients = null;
            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Calculating files..." });
            string[] calculationResults = new string[this.probeRawFileNames.Length];
            for (int i = 0; i < this.probeRawFileNames.Length; i++) 
            {
                //Calculate the coefficients
                if (this.probeRawFileNames[i] != null)
                {
                    coefficients = calculateFile(this.referenceFileName, this.probeRawFileNames[i]);
                    if (coefficients!=null)
                    {
                        calculationResults[i] = "PASS";
                    }
                    else 
                    {
                        calculationResults[i] = "FAIL--CALC";
                    }
                }

                //Write the coefficients
                if (coefficients != null) 
                {
                    try
                    {
                        if (calculationResults[i].Contains("PASS"))
                        {
                            writeCalFiles(this.probeRawFileNames[i], coefficients);
                        }
                        
                    }
                    catch 
                    {
                        calculationResults[i] += "FAIL--WRITE";
                    }
                }
                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Calibration calculated successfully." });
            }

            System.Threading.Thread.Sleep(1000);
            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Analyzing calibration..." });

            //Construct a string with the analysis results
            string sAnalysis = "ProbeID\tRESULT\r\n------------------------\r\n";

            for (int i = 0; i < this.numberOfPanels*10; i++) 
            {
                ulong probeNumber = this.firstProbeID + (ulong)i;
                string probeName = "P" + probeNumber.ToString();
                int probeIndex = -1;
                //Find the calculation result to go along with the probe
                for (int j = 0; j < this.probeRawFileNames.Length; j++) 
                {
                    if (this.probeRawFileNames[j] != null) 
                    {
                        if (this.probeRawFileNames[j].Contains(probeName))
                        {
                            probeIndex = j;
                            break;
                        }
                    }
                }
                if (probeIndex != -1 && !calculationResults[probeIndex].Contains("FAIL"))
                {
                    string calFileName = this.probeRawFileNames[i].Replace(".at_raw", "_AT.tcal");
                    int endOfDirectory = calFileName.LastIndexOf('\\');
                    sAnalysis += probeName + "\t" + analyzeCalibration(calFileName, this.probeRawFileNames[i], this.referenceFileName) + "\r\n";
                }
                else if (probeIndex == -1)
                {
                    sAnalysis += probeName + "\tFAIL--PROBE\r\n";
                }
                else 
                {
                    sAnalysis += probeName + "\t" + calculationResults[probeIndex];
                }
            }
            //Get the elapsed time of the procedure
            TimeSpan elaspedTime = DateTime.Now - this.timeDatum;

            //Write the results to a summary file
            System.IO.TextWriter summaryFile = new System.IO.StreamWriter(this.currentSetup.rootDirectory+"\\" + this.dateTimeFolder + "\\Summary.txt");
            summaryFile.WriteLine("Process Time: " + elaspedTime.TotalHours.ToString("0.000")+" hours");
            summaryFile.Write(sAnalysis);
            summaryFile.Close();

            System.Threading.Thread.Sleep(1000);
            this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Process has completed successfully." });
        }

        private bool connectToProbeMux() 
        {
            this.probeMux = new Agilent34980A(this.currentSetup.probeMuxIPAddress, this.currentSetup.probeMuxPort);
            return this.probeMux.Initialize();
        }

        private bool connectToChamber()
        {
            this.temperatureChamber = new Watlow(this.currentSetup.temperatureChamberCOMPort);

            if (!this.temperatureChamber.detectModule())
            {
                return false;
            }

            lock (_chamberLock)
            {
                this.loggingChamberData = true;
            }

            this.chamberMonitorThread = new System.Threading.Thread(chamberMonitor);
            this.chamberMonitorThread.Start();

            return true;
        }

        private bool connectToReferenceMux() 
        {
            this.referenceMux = new Agilent34970A(this.currentSetup.referenceMuxPort);
            return this.referenceMux.Init();
        }

        private bool connectToRelayBoard() 
        {
            this.relayBoard = new BBModule(this.currentSetup.relayBoardCOMPort);
            if (this.relayBoard.detectModule())
            {
                this.relayBoard.setOutput(0, false);
                this.relayBoard.setOutput(1, false);
                return true;
            }
            else 
            {
                return false;
            }
        }

        private string[,] verifyProbes() 
        {
            //Determine the number of valid probes by scanning each channel
            string[] muxChannels = new string[this.currentSetup.numberOfCards*60];
            string[,] newChannelMap = new string[muxChannels.Length, 3];

            //Name each channel based on the card
            int cardCounter = 1;
            while (cardCounter <= this.currentSetup.numberOfCards) 
            {
                for (int j = 1; j <= 60; j++)
                {
                    int channelName = 1000 * cardCounter + j;
                    //Fancy equation that just identifies all the possible channels on the mux (1001-1070,2001-2070,etc)
                    muxChannels[(cardCounter - 1) * 60 + (j - 1)] = channelName.ToString();
                }
                cardCounter++;
            }

            //Add the probe channels to the channel map
            for (int i = 0; i < muxChannels.Length; i++) 
            {
                newChannelMap[i, 0] = muxChannels[i];
            }

            //Determine the number of channels with probes attached
            this.probeMux.setChannelList(muxChannels);

            System.Threading.Thread.Sleep(1000);

            double[] resistanceValues = this.probeMux.readChannels();

            //Note that resistanceValues.Length = muxChannels.Length
            for (int i = 0; i < resistanceValues.Length; i++) 
            {
                if (resistanceValues[i] > 1000.0 && resistanceValues[i] < 40000.0)
                {
                    newChannelMap[i, 1] = "YES"; //Set column to "YES" if resistance in range
                }
                else 
                {
                    newChannelMap[i, 1] = "NO"; //Set column to "NO" if resistance in range
                }
            }

            return newChannelMap;

        }

        private void initializeTextFiles() 
        {
            ProbeID newForm = new ProbeID();
            newForm.dataPasser = this;
            this.childFormClose = new System.Threading.AutoResetEvent(false);
            newForm.ShowDialog();

            this.childFormClose.WaitOne();
            
            //Cancel the calibration if the user has requested
            this.readyForCalibration = newForm.success;
            if (!newForm.success) 
            {
                return;
            }

            DateTime newFolderTime = DateTime.Now;
            dateTimeFolder = newFolderTime.Year.ToString() + "_" + newFolderTime.Month.ToString("00") + "_" + newFolderTime.Day.ToString("00") + "_" + newFolderTime.Hour.ToString("00") + newFolderTime.Minute.ToString("00");
            
            System.IO.Directory.CreateDirectory(this.currentSetup.rootDirectory+"\\" + dateTimeFolder);

            //Initialize Reference File
            this.referenceFileName = this.currentSetup.rootDirectory+"\\" + dateTimeFolder + "\\Agilent34970A.t_ref";
            this.referenceFile = new System.IO.StreamWriter(this.referenceFileName);

            this.referenceFile.WriteLine("Air Temperature Calibration Process -- Reference Temperatue Data\r\n");
            this.referenceFile.WriteLine("Date Time: " + DateTime.Now + "\r\n");
            this.referenceFile.WriteLine("TimeStamp(sec),Temp(C)");
            
            //Name the probes
            string[] probeNames = new string[numberOfPanels * 10];
            for (int i = 0; i < probeNames.Length; i++) 
            {
                probeNames[i] = "P"+(this.firstProbeID + (UInt64)i).ToString();   
            }

            //Add the probes to the channel map
            for (int i = 0; i < this.channelMap.Length/3; i++) //Divide by 3 to get the number of rows
            {
                if (i < probeNames.Length)
                {
                    this.channelMap[i, 2] = probeNames[i] + ".at_raw";
                }
            }

            //Create the new text files for each probe
            this.probeRawFileNames = new string[this.channelMap.Length / 3];
            this.probeRawFiles = new System.IO.TextWriter[this.channelMap.Length / 3];

            for (int i = 0; i < this.channelMap.Length / 3; i++) 
            {
                if (this.channelMap[i, 1] == "YES")
                {
                    this.probeRawFileNames[i] = this.currentSetup.rootDirectory+"//" + dateTimeFolder + "\\" + this.channelMap[i, 2];
                    this.probeRawFiles[i] = new System.IO.StreamWriter(this.probeRawFileNames[i]);
                    
                    //Write header information
                    this.probeRawFiles[i].WriteLine("Air Temperature Calibration Process -- Air Temperatue Data\r\n");
                    this.probeRawFiles[i].WriteLine("Date Time: " + DateTime.Now + "\r\n");
                    this.probeRawFiles[i].WriteLine("TimeStamp(sec),Resistance(Ohms)");

                    //this.probeRawFiles[i].Close();
                }
            }
        }

        public void referenceMonitor()
        {
            double temperatureSetpoint;
            this.loggingReference = false;

            System.Threading.Thread.CurrentThread.IsBackground = true;

            double[] stabilityArray = new double[120];
            int i = 0;
            bool stabilityReady = false;
            //bool heatersOn = false;
            //bool heaterToggle = true;

            while (!this.IsDisposed)
            {
                lock (_chamberLock)
                {
                    temperatureSetpoint = this.currentTemperatureSetpoint;
                }
                if (!stabilityReady && i == stabilityArray.Length)
                {
                    stabilityReady = true;
                    i = 0;
                }
                if (i == stabilityArray.Length)
                {
                    i = 0;
                }

                double newResistance = this.referenceMux.read4WireResistance();
                double newTemperature = calculateReferenceTemp(newResistance);

                lock (_referenceLock) 
                {
                    this.currentTemperature = newTemperature;
                }

                if (this.referenceDataIn != null)
                {
                    this.referenceDataIn.Set();
                }

                bool logging;
                lock (_referenceLock)
                {
                    logging = this.loggingReference;
                }
                if (logging)
                {
                    TimeSpan elapsedTime = DateTime.Now - this.timeDatum;
                    if (newResistance != 0 && this.referenceFile != null)
                    {
                        this.referenceFile.WriteLine(elapsedTime.TotalSeconds.ToString("0.000") + "," + newTemperature.ToString("0.000"));
                    }
                }

                if (stabilityReady)
                {
                    double deviation = calculateStDev(stabilityArray);

                    if (deviation < this.currentSetup.referenceStabilityLimit && this.environmentStable != null && newTemperature > this.currentTemperatureSetpoint - 3.0 && newTemperature < this.currentTemperatureSetpoint + 3.0)
                    {
                        this.environmentStable.Set();
                    }
                    try
                    {
                        this.Invoke(new UpdateReferenceData(this.UpdateReferenceTextBoxes), new object[] { newResistance.ToString(), newTemperature.ToString("0.000"), deviation.ToString("0.0000") });
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        this.Invoke(new UpdateReferenceData(this.UpdateReferenceTextBoxes), new object[] { newResistance.ToString(), newTemperature.ToString("0.000"), "Waiting..." });
                    }
                    catch { }
                }

                stabilityArray[i] = newTemperature;

                i++;

                System.Threading.Thread.Sleep(900);
            }
        }

        private void chamberMonitor()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            bool readingChamber;
            double[] stabilityArray = new double[60];
            bool stablityReady = false;
            int i = 0;

            lock (_chamberLock)
            {
                readingChamber = this.loggingChamberData;
            }
            System.Threading.Thread.Sleep(2000);

            while (readingChamber)
            {
                double setPoint = (double)this.temperatureChamber.getAirTempSetPoint() / 10.0;
                double currentTemp = (double)this.temperatureChamber.getAirTemp() / 10.0;

                try
                {
                    this.Invoke(new UpdateChamber(this.UpdateChamberTextBoxes), new object[] { setPoint.ToString("0.0"), currentTemp.ToString("0.0") });
                }
                catch { }

                stabilityArray[i] = currentTemp;

                if ((i == 59)&&!stablityReady) 
                {
                    stablityReady = true;
                    i = 0;
                }
                if ( i == 59) 
                {
                    i = 0;
                }
                if (stablityReady)
                {
                    double stability = calculateStDev(stabilityArray);
                    this.Invoke(new UpdateChamberStability(this.UpdateChamberStabilityBox), new object[] { stability.ToString("0.000") });
                }
                else 
                {
                    this.Invoke(new UpdateChamberStability(this.UpdateChamberStabilityBox), new object[] { "Waiting..."}); 
                }
                          
                this.Invoke(new UpdateChamber(this.UpdateChamberTextBoxes), new object[] { setPoint.ToString("0.0"), currentTemp.ToString("0.0") });

                System.Threading.Thread.Sleep(1000);
                
                lock (_chamberLock)
                {
                    readingChamber = this.loggingChamberData;
                }

                i++;
            }
        }

        private double calculateReferenceTemp(double resistance) 
        {
            //Steinhart-Hart coefficients for Probe 006 Thermometrics
            double a0 = this.currentSetup.referenceA0;
            double a1 = this.currentSetup.referenceA1;
            double a2 = this.currentSetup.referenceA2;
            double a3 = this.currentSetup.referenceA3;
            double a4 = this.currentSetup.referenceA4;
            double a5 = this.currentSetup.referenceA5;

            double lnRes = Math.Log(resistance,Math.E);
            double invTemp = a0 + a1 * lnRes + a2 * Math.Pow(lnRes, 2) + a3 * Math.Pow(lnRes, 3) + a4 * Math.Pow(lnRes, 4) + a5 * Math.Pow(lnRes, 5);
            double kT = 1 / invTemp;
            return kT - 273.15;
        }

        private double calculateTempSteinhartHart(double resistance, double a0, double a1, double a2, double a3) 
        {
            double lnRes = Math.Log(resistance, Math.E);
            double invTemp = a0 + a1 * lnRes + a2 * Math.Pow(lnRes, 2) + a3 * Math.Pow(lnRes, 3);
            double kT = 1 / invTemp;
            return kT - 273.15;
        }

        private void UpdateStatusBoxMessage(string status)
        {
            textBoxStatus.Text = status;
        }

        private void UpdateChamberTextBoxes(string setpoint, string temperature)
        {
            textBoxChamberSetpoint.Text = setpoint;
            textBoxChamberTemperature.Text = temperature;
        }

        private void UpdateChamberStabilityBox(string stability) 
        {
            textBoxChamberStability.Text = stability;
        }

        private void UpdateReferenceTextBoxes(string resistance, string temperature,string stability) 
        {
            textBoxRefResistance.Text = resistance;
            textBoxRefTemp.Text = temperature;
            textBoxRefStability.Text = stability;
        }

        private void UpdateButtonsEnabled(bool initializeButton, bool calibrationButton) 
        {
            buttonConnect.Enabled = initializeButton;
            buttonStart.Enabled = calibrationButton;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.currentSetup = new Setup();
            try
            {
                this.currentSetup.getSetupFromFile(@"C:\Temperature Calibration\Setup.cfg");
            }
            catch 
            {
                MessageBox.Show("Configuration file has been modified or destroyed. The process must be terminated.", "Critical Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            this.currentSetup.setFileFromSetup();
            
            //Reference Steinhart-Hart Coefficients
            /*
            this.currentSetup.referenceA0 = 1.493995254478e-4;
            this.currentSetup.referenceA1 = 9.246533453364e-4;
            this.currentSetup.referenceA2 = -1.547539376530e-4;
            this.currentSetup.referenceA3 = 1.826585230302e-5;
            this.currentSetup.referenceA4 = -1.055047354655e-6;
            this.currentSetup.referenceA5 = 2.447072668566e-8;
            */
        }

        public double calculateStDev(double[] numberList)
        {
            double arraySum = 0;
            foreach (double number in numberList)
            {
                arraySum += number;
            }
            double mean = arraySum / numberList.Length;
            double sumOfDevSquared = 0;
            foreach (double number in numberList)
            {
                sumOfDevSquared += Math.Pow((number - mean), 2);
            }
            return Math.Sqrt(sumOfDevSquared / (numberList.Length - 1));
        }

        public double calculateMean(double[] numberList)
        {
            double arraySum = 0;

            foreach (double number in numberList)
            {
                arraySum += number;
            }

            return arraySum / numberList.Length;
        }

        private void goToTemperatureSetpoint(double temperature) 
        {
            //Turn off the thread for monitoring the chamber so the command can be sent
            lock (_chamberLock) 
            {
                this.loggingChamberData = false;
                this.currentTemperatureSetpoint = temperature;
            }
            //Wait for chamber thread to terminate
            while (this.chamberMonitorThread.IsAlive) 
            {
                System.Threading.Thread.Sleep(100);
            }

            //Translate the setpoint temperature to the actual chamber temperature
            double translatedTemperature;
        
            double a4 = -1e-6;
            double a3 = -8e-6;
            double a2 = 3.4e-3;
            double a1 = 1.043;
            double a0 = -5.3597;
            
            translatedTemperature = a4 * Math.Pow(temperature, 4) + a3 * Math.Pow(temperature, 3) + a2 * Math.Pow(temperature, 2) + a1 * temperature + a0;
            //translatedTemperature = temperature;
            
  
            //Set the chamber to the translated temperature so it achieves actual temperature
            this.temperatureChamber.setAirTemp(translatedTemperature);

            //Turn the chamber logging back on by setting the variable to "true" and starting the thread
            lock (_chamberLock) 
            {
                this.loggingChamberData = true;
            }
            this.chamberMonitorThread = new System.Threading.Thread(this.chamberMonitor);
            this.chamberMonitorThread.Start();

            //Wait for environment to become stable before ending the function
            this.environmentStable = new System.Threading.AutoResetEvent(false);
            this.environmentStable.WaitOne();
        }

        private void logCalibrationData(int numberOfDataPoints) 
        {
            while (numberOfDataPoints > 0)
            {
                lock (_referenceLock)
                {
                    this.loggingReference = true;
                }

                //Wait for first reference data to log
                this.referenceDataIn = new System.Threading.AutoResetEvent(false);
                this.referenceDataIn.WaitOne();

                this.Invoke(new UpdateStatus(this.UpdateStatusBoxMessage), new object[] { "Logging calibration data..."+numberOfDataPoints.ToString() });

                TimeSpan startReadTime = DateTime.Now - this.timeDatum;
                double[] newResistances = null;
                try
                {
                    newResistances = this.probeMux.readChannels();
                }
                catch 
                {
                    numberOfDataPoints++;
                    newResistances = null;
                }
                TimeSpan endReadTime = DateTime.Now - this.timeDatum;

                for (int i = 0; i < this.probeRawFiles.Length; i++)
                {
                    if (this.probeRawFiles[i] != null && newResistances!=null)
                    {
                        //Linearly interpolate the actual time of the read
                        double calculatedTime=i*(endReadTime.TotalSeconds-startReadTime.TotalSeconds)/this.probeRawFiles.Length+startReadTime.TotalSeconds;
                        this.probeRawFiles[i].WriteLine(calculatedTime.ToString("0.000") + "," + newResistances[i].ToString("0.0"));
                    }
                }

                //System.Threading.Thread.Sleep(500);
                numberOfDataPoints--;
            }
            lock (_referenceLock)
            {
                this.loggingReference = false;
            }
        }

        private void closeLogFiles() 
        {
            //Close the reference file
            this.referenceFile.Close();

            //Close all the probe raw files
            for (int i = 0; i < this.probeRawFiles.Length; i++) 
            {
                if (this.probeRawFiles[i] != null) 
                {
                    this.probeRawFiles[i].Close();   
                }
            }
        }

        private double[] calculateFile(string referenceFileName, string rawProbeFileName) 
        {
            string[] referenceFileAllLines = System.IO.File.ReadAllLines(referenceFileName);
            string[] rawProbeFileAllLines = System.IO.File.ReadAllLines(rawProbeFileName);
            int numberOfHeaderLines = 5;

            //Read the reference data file
            List<string> sReferenceData = new List<string>();
            for (int i = numberOfHeaderLines; i < referenceFileAllLines.Length; i++) 
            {
                sReferenceData.Add(referenceFileAllLines[i]);
            }
            double[,] dReferenceData = new double[sReferenceData.Count, 2];
            for (int i = 0; i < sReferenceData.Count; i++) 
            {
                string[] splitLine = sReferenceData[i].Split(',');
                dReferenceData[i, 0] = double.Parse(splitLine[0]);
                dReferenceData[i, 1] = double.Parse(splitLine[1]);
            }
            
            //Read the probe data file
            List<string> sProbeRawData = new List<string>();
            for (int i = numberOfHeaderLines; i < rawProbeFileAllLines.Length; i++) 
            {
                sProbeRawData.Add(rawProbeFileAllLines[i]);
            }
            double[,] dProbeRawData = new double[sProbeRawData.Count, 2];
            for (int i = 0; i < sProbeRawData.Count; i++) 
            {
                string[] splitLine = sProbeRawData[i].Split(',');
                dProbeRawData[i, 0] = double.Parse(splitLine[0]);
                dProbeRawData[i, 1] = double.Parse(splitLine[1]);
            }

            //Time sync the data
            List<double[]> timeSyncedData = new List<double[]>();

            for (int i = 0; i < dProbeRawData.Length/2; i++) 
            {
                for (int j = 0; j < dReferenceData.Length/2; j++) 
                {
                    if (dProbeRawData[i, 0] >= dReferenceData[j, 0] && dProbeRawData[i, 0] <= dReferenceData[j + 1, 0]) 
                    {
                        if ((Math.Abs(dProbeRawData[i, 0] - dReferenceData[j, 0])) < Math.Abs(dProbeRawData[i, 0] - dReferenceData[j + 1, 0]))
                        {
                            double[] newPoint = new double[] { dProbeRawData[i, 0], dProbeRawData[i, 1], dReferenceData[j, 0], dReferenceData[j, 1] };
                            timeSyncedData.Add(newPoint);
                        }
                        else 
                        {
                            double[] newPoint = new double[] { dProbeRawData[i, 0], dProbeRawData[i, 1], dReferenceData[j+1, 0], dReferenceData[j+1, 1] };
                            timeSyncedData.Add(newPoint);   
                        }
                        break;
                    }
                }   
            }

            //Build the reference matrix
            double[] dReferenceVector = new double[timeSyncedData.Count];
            for (int i = 0; i < timeSyncedData.Count; i++) 
            {
                double[] refPoint = timeSyncedData[i];
                double kTemperature = refPoint[3]+ 273.15;//Convert reference temp to Kelvin
                dReferenceVector[i] = 1 / kTemperature;
            }
            Matrix mReferenceMatrix = new Matrix(timeSyncedData.Count, 1);
            for (int i = 0; i < timeSyncedData.Count; i++) 
            {
                mReferenceMatrix[i, 0] = dReferenceVector[i];
            }

            //Build the observation matrix
            double[,] dObservationMatrix = new double[timeSyncedData.Count, 3];
            for (int i = 0; i < timeSyncedData.Count; i++) 
            {
                double[] rawPoint = timeSyncedData[i];
                double probeResistance = rawPoint[1];
                dObservationMatrix[i, 0] = 1;
                dObservationMatrix[i, 1] = Math.Log(probeResistance, Math.E);
                dObservationMatrix[i, 2] = Math.Pow(Math.Log(probeResistance, Math.E), 3);
            }
            Matrix mObservationMatrix = Matrix.Create(dObservationMatrix);
            
            //Calculate the coefficients using a Least Squares Fit
            Matrix transposedO;
            Matrix coefficientMatrix;
            try
            {
                transposedO = Matrix.Transpose(mObservationMatrix);
                coefficientMatrix = (((transposedO.Multiply(mObservationMatrix)).Inverse()).Multiply(transposedO)).Multiply(mReferenceMatrix);
            }
            catch 
            {
                return null;
            }
            //Store to the coefficient array
            double[] coefficients = new double[3];
            for (int i = 0; i < coefficients.Length; i++) 
            {
                coefficients[i] = coefficientMatrix[i, 0];
            }
            return coefficients;
        }

        public string analyzeCalibration(string calFileName,string rawDataFileName,string referenceDataFileName) 
        {
            //Read the calibration file and get the coefficients
            string[] calFileReader = System.IO.File.ReadAllLines(calFileName);
            double[] coefficients = new double[] { double.Parse(calFileReader[1]), double.Parse(calFileReader[2]), double.Parse(calFileReader[3]) };

            //Read the reference data file
            string[] referenceFileAllLines = System.IO.File.ReadAllLines(referenceDataFileName);
            int numberOfHeaderLines = 5;
            List<string> sReferenceData = new List<string>();
            for (int i = numberOfHeaderLines; i < referenceFileAllLines.Length; i++)
            {
                sReferenceData.Add(referenceFileAllLines[i]);
            }
            double[,] dReferenceData = new double[sReferenceData.Count, 2];
            for (int i = 0; i < sReferenceData.Count; i++)
            {
                string[] splitLine = sReferenceData[i].Split(',');
                dReferenceData[i, 0] = double.Parse(splitLine[0]);
                dReferenceData[i, 1] = double.Parse(splitLine[1]);
            }

            //Read the probe data file
            string[] rawProbeFileAllLines = System.IO.File.ReadAllLines(rawDataFileName);
            List<string> sProbeRawData = new List<string>();
            for (int i = numberOfHeaderLines; i < rawProbeFileAllLines.Length; i++)
            {
                sProbeRawData.Add(rawProbeFileAllLines[i]);
            }
            double[,] dProbeRawData = new double[sProbeRawData.Count, 2];
            for (int i = 0; i < sProbeRawData.Count; i++)
            {
                string[] splitLine = sProbeRawData[i].Split(',');
                dProbeRawData[i, 0] = double.Parse(splitLine[0]);
                dProbeRawData[i, 1] = double.Parse(splitLine[1]);
            }

            //Time sync the data
            List<double[]> timeSyncedData = new List<double[]>();
            for (int i = 0; i < dProbeRawData.Length / 2; i++)
            {
                for (int j = 0; j < dReferenceData.Length / 2; j++)
                {
                    if (dProbeRawData[i, 0] >= dReferenceData[j, 0] && dProbeRawData[i, 0] <= dReferenceData[j + 1, 0])
                    {
                        if ((Math.Abs(dProbeRawData[i, 0] - dReferenceData[j, 0])) < Math.Abs(dProbeRawData[i, 0] - dReferenceData[j + 1, 0]))
                        {
                            double[] newPoint = new double[] { dProbeRawData[i, 0], dProbeRawData[i, 1], dReferenceData[j, 0], dReferenceData[j, 1] };
                            timeSyncedData.Add(newPoint);
                        }
                        else
                        {
                            double[] newPoint = new double[] { dProbeRawData[i, 0], dProbeRawData[i, 1], dReferenceData[j + 1, 0], dReferenceData[j + 1, 1] };
                            timeSyncedData.Add(newPoint);
                        }
                        break;
                    }
                }
            }
            //
            //Perform the calculations on the time-synced data
            //
            //Calculate the temperature for each data point using the Steinhart-Hart Equation and the coefficients
            double[,] calculatedTemperatureData = new double[timeSyncedData.Count / 4,2];
            for (int i = 0; i < timeSyncedData.Count / 4; i++) 
            {
                //Get the resistance from column [1] of the time synced data
                double[] timeSyncedDataLine = timeSyncedData[i];
                double resistance = timeSyncedDataLine[1];
                //Calculate the temperature using Steinhart-Hart
                double temperature = calculateTempSteinhartHart(resistance, coefficients[0], coefficients[1], 0, coefficients[2]);
                calculatedTemperatureData[i, 0] = temperature;
                //Calculate the difference between the calculated temperature and reference temperature (column[3])
                double temperatureError = calculatedTemperatureData[i, 0] - timeSyncedDataLine[3];
                calculatedTemperatureData[i, 1] = temperatureError;
            }
            //Get the average error, mean error, and RMS error from the calculations
            double[] errorArray = new double[calculatedTemperatureData.Length / 2];
            for (int i = 0; i < errorArray.Length; i++) 
            {
                errorArray[i] = calculatedTemperatureData[i, 1];
            }
            double averageError = calculateMean(errorArray);
            double deviation = calculateStDev(errorArray);
            double RMSerror = Math.Sqrt(Math.Pow(averageError, 2) + Math.Pow(deviation, 2));

            //Return a string with the calculations
            string message = "";
            if (RMSerror <= 0.3)
            { message += "PASS\t"; }
            else
            { message += "FAIL\t"; }
            message += "Mean: " + averageError.ToString("0.000") + "\tDeviation: " + deviation.ToString("0.000") + "\tRMS: " + RMSerror.ToString("0.000");

            return message;
        }

        private void writeCalFiles(string rawFileName, double[] coefficients) 
        {
            string fileName;
            string probeNumber = System.IO.Path.GetFileNameWithoutExtension(rawFileName);

            if (rawFileName.Contains(".at_raw"))
            {
                fileName = rawFileName.Replace(".at_raw", "_AT.tcal");
            }
            else 
            {
                fileName = rawFileName.Replace(".ut_raw", "_UT.tcal");
            }
            
            System.IO.TextWriter calFile = new System.IO.StreamWriter(fileName);

            calFile.WriteLine(probeNumber);
            
            for (int i = 0; i < coefficients.Length; i++) 
            {
                calFile.WriteLine(coefficients[i].ToString("+0.00000000e+00;-0.00000000e+00"));
            }

            calFile.Close();           
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            this.RunCalibration = new BackgroundWorker();
            RunCalibration.DoWork+=new DoWorkEventHandler(RunCalibration_DoWork);
            RunCalibration.RunWorkerAsync();
        }

        private void calculateCoefficientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog rawFilesDialog = new OpenFileDialog();
            rawFilesDialog.Multiselect = true;
            rawFilesDialog.InitialDirectory = this.currentSetup.rootDirectory;
            rawFilesDialog.Title = "Radiosonde Raw Files";
            rawFilesDialog.Filter = "Air Temp Raw Files(*.at_raw)|*.at_raw|Humidity Temp Raw Files(*.ut_raw)|*ut_raw";
            
            //Get the file names for calculations from the user selected files
            string[] fileNames = null;
            if (rawFilesDialog.ShowDialog() == DialogResult.OK)
            {
                fileNames = rawFilesDialog.FileNames;
            }
            else { return; }

            //Check for the reference file and notify user if it cannot be found
            int startTrim = fileNames[0].LastIndexOf("\\");
            string currentDirectory = fileNames[0].Remove(startTrim);
            string referenceFileName = currentDirectory + "\\Agilent34970A.t_ref";

            if (!System.IO.File.Exists(referenceFileName)) 
            {
                MessageBox.Show("Reference file " + referenceFileName + " could not be found for the selected files.", "Reference File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            foreach (string rawFile in fileNames) 
            {
                double[] coefficients = calculateFile(referenceFileName, rawFile);
                writeCalFiles(rawFile, coefficients);                
            }

            MessageBox.Show("All coefficients have been successfully generated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void analyzedCalibrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog rawFilesDialog = new OpenFileDialog();
            rawFilesDialog.Multiselect = true;
            rawFilesDialog.InitialDirectory = this.currentSetup.rootDirectory;
            rawFilesDialog.Title = "Radiosonde Raw Files";
            rawFilesDialog.Filter = "Air Temp Raw Files(*.at_raw)|*.at_raw|Humidity Temp Raw Files(*.ut_raw)|*ut_raw";

            //Get the file names for calculations from the user selected files
            string[] fileNames = null;
            if (rawFilesDialog.ShowDialog() == DialogResult.OK)
            {
                fileNames = rawFilesDialog.FileNames;
            }
            else { return; }

            //Check for the reference file and notify user if it cannot be found
            int startTrim = fileNames[0].LastIndexOf("\\");
            string currentDirectory = fileNames[0].Remove(startTrim);
            string referenceFileName = currentDirectory + "\\Agilent34970A.t_ref";

            if (!System.IO.File.Exists(referenceFileName))
            {
                MessageBox.Show("Reference file " + referenceFileName + " could not be found for the selected files.", "Reference File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string userMessage = "PROBE ID\t\tRESULT\r\n";

            //Analyze each file
            foreach (string file in fileNames)
            {
                string calFileName = null;
                if (file.Contains(".at_raw"))
                {
                    calFileName = file.Replace(".at_raw", "_AT.tcal");
                }
                else if (file.Contains(".ut_raw"))
                {
                    calFileName = file.Replace(".ut_raw", "_UT.tcal");
                }
                string probeID = calFileName.Remove(0, calFileName.LastIndexOf("\\"));
                probeID = probeID.Trim('\\');
                probeID = probeID.Replace(".tcal", null);

                string newResult = probeID + "\t" + analyzeCalibration(calFileName, file, referenceFileName);
                userMessage += newResult + "\r\n";
            }

            MessageBox.Show(userMessage, "Calibration Analysis", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void portConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            PortConfigurationForm newConfiguration = new PortConfigurationForm();
            newConfiguration.dataPasser = this;
            newConfiguration.Show();
            newConfiguration.FormClosed += new FormClosedEventHandler(newConfiguration_FormClosed);
        }

        void newConfiguration_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.currentSetup.getSetupFromFile(@"C:\Temperature Calibration\Setup.cfg");
            this.Enabled = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.relayBoard.setOutput(0, false);
                this.relayBoard.setOutput(1, false);

                foreach (System.IO.TextWriter file in this.probeRawFiles) 
                {
                    if (file != null) 
                    {
                        file.Dispose();
                    }
                }
                if (this.referenceFile != null) 
                {
                    this.referenceFile.Dispose();
                }
            }
            catch { return; }
        }

    }

    public class Setup 
    {
        //Root directory for logging calibration
        public string rootDirectory;
        //COM port for the reference multiplexer
        public string referenceMuxPort;
        //Reference Sensor Coefficients
        public double referenceA0;
        public double referenceA1;
        public double referenceA2;
        public double referenceA3;
        public double referenceA4;
        public double referenceA5;
        //COM port for the temperature chamber
        public string temperatureChamberCOMPort;
        //COM port for the relay board
        public string relayBoardCOMPort;
        //IP configuration for the probe multiplexer
        public string probeMuxIPAddress;
        public int probeMuxPort;
        public int numberOfCards;
        //Calibration Information
        public double chamberStabilityLimit;
        public double referenceStabilityLimit;
        public double[] temperatureSetpoints;
        public int numberOfDataPoints;
        
        public void getSetupFromFile(string fileName) 
        {
            //Read all the lines of the config file
            string[] configFileAllLines = System.IO.File.ReadAllLines(fileName);
            string[] splitLine;
            
            //Get the root directory
            splitLine = configFileAllLines[0].Split('\t');
            string newRootDirectory = splitLine[1];
            this.rootDirectory = newRootDirectory;
            
            //Get the reference sensor COM port
            splitLine = configFileAllLines[1].Split('\t');
            string newReferenceMuxPort = splitLine[1];
            this.referenceMuxPort = newReferenceMuxPort;
            
            //Get the reference sensor Steinhart-Hart Coefficients
            splitLine = configFileAllLines[2].Split('\t');
            double newReferenceA0 = double.Parse(splitLine[1]);
            this.referenceA0 = newReferenceA0;
            splitLine = configFileAllLines[3].Split('\t');
            double newReferenceA1 = double.Parse(splitLine[1]);
            this.referenceA1 = newReferenceA1;
            splitLine = configFileAllLines[4].Split('\t');
            double newReferenceA2 = double.Parse(splitLine[1]);
            this.referenceA2 = newReferenceA2;
            splitLine = configFileAllLines[5].Split('\t');
            double newReferenceA3 = double.Parse(splitLine[1]);
            this.referenceA3 = newReferenceA3;
            splitLine = configFileAllLines[6].Split('\t');
            double newReferenceA4 = double.Parse(splitLine[1]);
            this.referenceA4 = newReferenceA4;
            splitLine = configFileAllLines[7].Split('\t');
            double newReferenceA5 = double.Parse(splitLine[1]);
            this.referenceA5 = newReferenceA5;

            //Get the temperature chamber COM port
            splitLine = configFileAllLines[8].Split('\t');
            string newTemperatureChamberCOMPort = splitLine[1];
            this.temperatureChamberCOMPort = newTemperatureChamberCOMPort;

            //Get the relay board COM port
            splitLine = configFileAllLines[9].Split('\t');
            string newRelayBoardCOMPort = splitLine[1];
            this.relayBoardCOMPort = newRelayBoardCOMPort;

            //Get the Probe Multiplexer Port Information
            splitLine = configFileAllLines[10].Split('\t');
            string newProbeMuxIPAddress = splitLine[1];
            this.probeMuxIPAddress = newProbeMuxIPAddress;
            splitLine = configFileAllLines[11].Split('\t');
            int newProbeMuxPort = int.Parse(splitLine[1]);
            this.probeMuxPort = newProbeMuxPort;
            splitLine = configFileAllLines[12].Split('\t');
            int newNumberOfCards = int.Parse(splitLine[1]);
            this.numberOfCards = newNumberOfCards;

            //Get the temperature setpoints
            splitLine = configFileAllLines[13].Split('\t');
            string[] sTemperatureSetpoints = splitLine[1].Split(',');
            this.temperatureSetpoints = new double[sTemperatureSetpoints.Length];
            for (int i = 0; i < temperatureSetpoints.Length; i++) 
            {
                temperatureSetpoints[i] = double.Parse(sTemperatureSetpoints[i]);
            }
            
            //Get the number of data points
            splitLine = configFileAllLines[14].Split('\t');
            int newNumberOfDataPoints = int.Parse(splitLine[1]);
            this.numberOfDataPoints = newNumberOfDataPoints;

            //Get the stability limit
            splitLine = configFileAllLines[15].Split('\t');
            double newStabilityLimit = double.Parse(splitLine[1]);
            this.referenceStabilityLimit = newStabilityLimit;
        }

        public void setFileFromSetup() 
        {
            System.IO.TextWriter newConfigFile = new System.IO.StreamWriter(@"C:\Temperature Calibration\Setup.tmp");
            //Write Root Directory
            newConfigFile.WriteLine("Root_Directory\t" + this.rootDirectory);
            newConfigFile.WriteLine("Reference_Port\t" + this.referenceMuxPort);
            newConfigFile.WriteLine("Coefficient_A0\t" + this.referenceA0.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Coefficient_A1\t" + this.referenceA1.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Coefficient_A2\t" + this.referenceA2.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Coefficient_A3\t" + this.referenceA3.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Coefficient_A4\t" + this.referenceA4.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Coefficient_A5\t" + this.referenceA5.ToString("+0.0000000000e+0;-0.0000000000e+0"));
            newConfigFile.WriteLine("Chamber_Port\t" + this.temperatureChamberCOMPort);
            newConfigFile.WriteLine("RelayBoard\t" + this.relayBoardCOMPort);
            newConfigFile.WriteLine("ProbeMux_IP\t"+this.probeMuxIPAddress);
            newConfigFile.WriteLine("ProbeMux_Port\t" + this.probeMuxPort.ToString());
            newConfigFile.WriteLine("NumberOfCards\t" + this.numberOfCards.ToString());

            newConfigFile.Write("Temperature_SP\t");
            for (int i = 0; i < this.temperatureSetpoints.Length-1; i++) 
            {
                newConfigFile.Write(this.temperatureSetpoints[i].ToString("#.0") + ",");
            }
            newConfigFile.Write(this.temperatureSetpoints[this.temperatureSetpoints.Length-1].ToString("#.0") + "\r\n");
            newConfigFile.WriteLine("DataPoints\t" + this.numberOfDataPoints.ToString());
            newConfigFile.WriteLine("Stability\t"+this.referenceStabilityLimit.ToString("0.00"));
            newConfigFile.Close();

            System.IO.File.Copy(@"C:\Temperature Calibration\Setup.tmp", @"C:\Temperature Calibration\Setup.cfg", true);
            System.IO.File.Delete(@"C:\Temperature Calibration\Setup.tmp");
        }

    }


    

}
