﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Temperature_Calibration
{
    public partial class ProbeID : Form
    {
        public ProbeID()
        {
            InitializeComponent();
        }

        public Form1 dataPasser;
        public UInt64 firstProbeID;
        public int numberOfPanels;
        public bool success;
    


        private void buttonOK_Click(object sender, EventArgs e)
        {
            UInt64 firstProbeNumber;
            int newNumberOfPanels;

            try
            {
                firstProbeNumber = UInt64.Parse(textBoxFirstProbeID.Text);
                this.firstProbeID = firstProbeNumber;
                this.dataPasser.firstProbeID = firstProbeNumber;
            }
            catch 
            {
                MessageBox.Show("First probe ID must be a positive integer.", "Invalid First Probe ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int maximumPanels = 0;

            try
            {
                newNumberOfPanels = int.Parse(textBoxPanels.Text);

                //Get the maximum number of panels based on the number of cards connected to system
                maximumPanels = this.dataPasser.currentSetup.numberOfCards * 60 / 10;

                if (newNumberOfPanels < 1 || newNumberOfPanels > maximumPanels) 
                {
                    MessageBox.Show("Number of panels must be greater than zero and less than "+maximumPanels.ToString()+".", "Invalid First Probe ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.numberOfPanels = newNumberOfPanels;
                this.dataPasser.numberOfPanels = newNumberOfPanels;
            }
            catch 
            {
                MessageBox.Show("Number of panels must be an integer greater than zero and less than " + maximumPanels.ToString() + ".", "Invalid First Probe ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            success = true;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            success = false;
            this.Close();
        }

        private void ProbeID_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.dataPasser.probeNames = this.probeNames;
            this.dataPasser.childFormClose.Set();
        }

    }
}
