﻿namespace Temperature_Calibration
{
    partial class PortConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxRootDirectory = new System.Windows.Forms.GroupBox();
            this.buttonRootDirectory = new System.Windows.Forms.Button();
            this.textBoxRootDirectory = new System.Windows.Forms.TextBox();
            this.groupBoxReference = new System.Windows.Forms.GroupBox();
            this.labelRefA5 = new System.Windows.Forms.Label();
            this.labelRefA4 = new System.Windows.Forms.Label();
            this.labelRefA3 = new System.Windows.Forms.Label();
            this.labelRefA2 = new System.Windows.Forms.Label();
            this.labelRefA1 = new System.Windows.Forms.Label();
            this.labelRefA0 = new System.Windows.Forms.Label();
            this.textBoxRefA5 = new System.Windows.Forms.TextBox();
            this.textBoxRefA4 = new System.Windows.Forms.TextBox();
            this.textBoxRefA3 = new System.Windows.Forms.TextBox();
            this.textBoxRefA2 = new System.Windows.Forms.TextBox();
            this.textBoxRefA1 = new System.Windows.Forms.TextBox();
            this.textBoxRefA0 = new System.Windows.Forms.TextBox();
            this.labelCoefficients = new System.Windows.Forms.Label();
            this.labelReferencePort = new System.Windows.Forms.Label();
            this.comboBoxReferencePort = new System.Windows.Forms.ComboBox();
            this.folderBrowserDialogRoot = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBoxChamber = new System.Windows.Forms.GroupBox();
            this.labelChamberPort = new System.Windows.Forms.Label();
            this.comboBoxChamberPort = new System.Windows.Forms.ComboBox();
            this.groupBoxProbeMux = new System.Windows.Forms.GroupBox();
            this.textBoxCards = new System.Windows.Forms.TextBox();
            this.labelCards = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.textBoxProbeIP = new System.Windows.Forms.TextBox();
            this.labelProbeIPAddress = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBoxRelayBoard = new System.Windows.Forms.GroupBox();
            this.labelRelayBoardPort = new System.Windows.Forms.Label();
            this.comboBoxRelayBoardPort = new System.Windows.Forms.ComboBox();
            this.groupBoxRootDirectory.SuspendLayout();
            this.groupBoxReference.SuspendLayout();
            this.groupBoxChamber.SuspendLayout();
            this.groupBoxProbeMux.SuspendLayout();
            this.groupBoxRelayBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxRootDirectory
            // 
            this.groupBoxRootDirectory.Controls.Add(this.buttonRootDirectory);
            this.groupBoxRootDirectory.Controls.Add(this.textBoxRootDirectory);
            this.groupBoxRootDirectory.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRootDirectory.Name = "groupBoxRootDirectory";
            this.groupBoxRootDirectory.Size = new System.Drawing.Size(182, 46);
            this.groupBoxRootDirectory.TabIndex = 0;
            this.groupBoxRootDirectory.TabStop = false;
            this.groupBoxRootDirectory.Text = "Root Directory";
            // 
            // buttonRootDirectory
            // 
            this.buttonRootDirectory.Location = new System.Drawing.Point(151, 20);
            this.buttonRootDirectory.Name = "buttonRootDirectory";
            this.buttonRootDirectory.Size = new System.Drawing.Size(25, 22);
            this.buttonRootDirectory.TabIndex = 1;
            this.buttonRootDirectory.Text = "...";
            this.buttonRootDirectory.UseVisualStyleBackColor = true;
            this.buttonRootDirectory.Click += new System.EventHandler(this.buttonRootDirectory_Click);
            // 
            // textBoxRootDirectory
            // 
            this.textBoxRootDirectory.Location = new System.Drawing.Point(6, 20);
            this.textBoxRootDirectory.Name = "textBoxRootDirectory";
            this.textBoxRootDirectory.Size = new System.Drawing.Size(139, 20);
            this.textBoxRootDirectory.TabIndex = 0;
            // 
            // groupBoxReference
            // 
            this.groupBoxReference.Controls.Add(this.labelRefA5);
            this.groupBoxReference.Controls.Add(this.labelRefA4);
            this.groupBoxReference.Controls.Add(this.labelRefA3);
            this.groupBoxReference.Controls.Add(this.labelRefA2);
            this.groupBoxReference.Controls.Add(this.labelRefA1);
            this.groupBoxReference.Controls.Add(this.labelRefA0);
            this.groupBoxReference.Controls.Add(this.textBoxRefA5);
            this.groupBoxReference.Controls.Add(this.textBoxRefA4);
            this.groupBoxReference.Controls.Add(this.textBoxRefA3);
            this.groupBoxReference.Controls.Add(this.textBoxRefA2);
            this.groupBoxReference.Controls.Add(this.textBoxRefA1);
            this.groupBoxReference.Controls.Add(this.textBoxRefA0);
            this.groupBoxReference.Controls.Add(this.labelCoefficients);
            this.groupBoxReference.Controls.Add(this.labelReferencePort);
            this.groupBoxReference.Controls.Add(this.comboBoxReferencePort);
            this.groupBoxReference.Location = new System.Drawing.Point(12, 64);
            this.groupBoxReference.Name = "groupBoxReference";
            this.groupBoxReference.Size = new System.Drawing.Size(182, 259);
            this.groupBoxReference.TabIndex = 1;
            this.groupBoxReference.TabStop = false;
            this.groupBoxReference.Text = "Reference Multiplexer Settings";
            // 
            // labelRefA5
            // 
            this.labelRefA5.AutoSize = true;
            this.labelRefA5.Location = new System.Drawing.Point(9, 231);
            this.labelRefA5.Name = "labelRefA5";
            this.labelRefA5.Size = new System.Drawing.Size(20, 13);
            this.labelRefA5.TabIndex = 14;
            this.labelRefA5.Text = "A5";
            // 
            // labelRefA4
            // 
            this.labelRefA4.AutoSize = true;
            this.labelRefA4.Location = new System.Drawing.Point(9, 205);
            this.labelRefA4.Name = "labelRefA4";
            this.labelRefA4.Size = new System.Drawing.Size(20, 13);
            this.labelRefA4.TabIndex = 13;
            this.labelRefA4.Text = "A4";
            // 
            // labelRefA3
            // 
            this.labelRefA3.AutoSize = true;
            this.labelRefA3.Location = new System.Drawing.Point(9, 179);
            this.labelRefA3.Name = "labelRefA3";
            this.labelRefA3.Size = new System.Drawing.Size(20, 13);
            this.labelRefA3.TabIndex = 12;
            this.labelRefA3.Text = "A3";
            // 
            // labelRefA2
            // 
            this.labelRefA2.AutoSize = true;
            this.labelRefA2.Location = new System.Drawing.Point(9, 153);
            this.labelRefA2.Name = "labelRefA2";
            this.labelRefA2.Size = new System.Drawing.Size(20, 13);
            this.labelRefA2.TabIndex = 11;
            this.labelRefA2.Text = "A2";
            // 
            // labelRefA1
            // 
            this.labelRefA1.AutoSize = true;
            this.labelRefA1.Location = new System.Drawing.Point(9, 127);
            this.labelRefA1.Name = "labelRefA1";
            this.labelRefA1.Size = new System.Drawing.Size(20, 13);
            this.labelRefA1.TabIndex = 10;
            this.labelRefA1.Text = "A1";
            // 
            // labelRefA0
            // 
            this.labelRefA0.AutoSize = true;
            this.labelRefA0.Location = new System.Drawing.Point(9, 101);
            this.labelRefA0.Name = "labelRefA0";
            this.labelRefA0.Size = new System.Drawing.Size(20, 13);
            this.labelRefA0.TabIndex = 9;
            this.labelRefA0.Text = "A0";
            // 
            // textBoxRefA5
            // 
            this.textBoxRefA5.Location = new System.Drawing.Point(35, 228);
            this.textBoxRefA5.Name = "textBoxRefA5";
            this.textBoxRefA5.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA5.TabIndex = 8;
            // 
            // textBoxRefA4
            // 
            this.textBoxRefA4.Location = new System.Drawing.Point(35, 202);
            this.textBoxRefA4.Name = "textBoxRefA4";
            this.textBoxRefA4.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA4.TabIndex = 7;
            // 
            // textBoxRefA3
            // 
            this.textBoxRefA3.Location = new System.Drawing.Point(35, 176);
            this.textBoxRefA3.Name = "textBoxRefA3";
            this.textBoxRefA3.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA3.TabIndex = 6;
            // 
            // textBoxRefA2
            // 
            this.textBoxRefA2.Location = new System.Drawing.Point(35, 150);
            this.textBoxRefA2.Name = "textBoxRefA2";
            this.textBoxRefA2.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA2.TabIndex = 5;
            // 
            // textBoxRefA1
            // 
            this.textBoxRefA1.Location = new System.Drawing.Point(35, 124);
            this.textBoxRefA1.Name = "textBoxRefA1";
            this.textBoxRefA1.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA1.TabIndex = 4;
            // 
            // textBoxRefA0
            // 
            this.textBoxRefA0.Location = new System.Drawing.Point(35, 98);
            this.textBoxRefA0.Name = "textBoxRefA0";
            this.textBoxRefA0.Size = new System.Drawing.Size(101, 20);
            this.textBoxRefA0.TabIndex = 3;
            // 
            // labelCoefficients
            // 
            this.labelCoefficients.AutoSize = true;
            this.labelCoefficients.Location = new System.Drawing.Point(6, 73);
            this.labelCoefficients.Name = "labelCoefficients";
            this.labelCoefficients.Size = new System.Drawing.Size(130, 13);
            this.labelCoefficients.TabIndex = 2;
            this.labelCoefficients.Text = "Steinhart-Hart Coefficients";
            // 
            // labelReferencePort
            // 
            this.labelReferencePort.AutoSize = true;
            this.labelReferencePort.Location = new System.Drawing.Point(6, 43);
            this.labelReferencePort.Name = "labelReferencePort";
            this.labelReferencePort.Size = new System.Drawing.Size(53, 13);
            this.labelReferencePort.TabIndex = 1;
            this.labelReferencePort.Text = "COM Port";
            // 
            // comboBoxReferencePort
            // 
            this.comboBoxReferencePort.FormattingEnabled = true;
            this.comboBoxReferencePort.Location = new System.Drawing.Point(75, 40);
            this.comboBoxReferencePort.Name = "comboBoxReferencePort";
            this.comboBoxReferencePort.Size = new System.Drawing.Size(86, 21);
            this.comboBoxReferencePort.TabIndex = 0;
            // 
            // groupBoxChamber
            // 
            this.groupBoxChamber.Controls.Add(this.labelChamberPort);
            this.groupBoxChamber.Controls.Add(this.comboBoxChamberPort);
            this.groupBoxChamber.Location = new System.Drawing.Point(200, 84);
            this.groupBoxChamber.Name = "groupBoxChamber";
            this.groupBoxChamber.Size = new System.Drawing.Size(182, 66);
            this.groupBoxChamber.TabIndex = 2;
            this.groupBoxChamber.TabStop = false;
            this.groupBoxChamber.Text = "Chamber Settings";
            // 
            // labelChamberPort
            // 
            this.labelChamberPort.AutoSize = true;
            this.labelChamberPort.Location = new System.Drawing.Point(11, 31);
            this.labelChamberPort.Name = "labelChamberPort";
            this.labelChamberPort.Size = new System.Drawing.Size(53, 13);
            this.labelChamberPort.TabIndex = 1;
            this.labelChamberPort.Text = "COM Port";
            // 
            // comboBoxChamberPort
            // 
            this.comboBoxChamberPort.FormattingEnabled = true;
            this.comboBoxChamberPort.Location = new System.Drawing.Point(70, 28);
            this.comboBoxChamberPort.Name = "comboBoxChamberPort";
            this.comboBoxChamberPort.Size = new System.Drawing.Size(86, 21);
            this.comboBoxChamberPort.TabIndex = 0;
            // 
            // groupBoxProbeMux
            // 
            this.groupBoxProbeMux.Controls.Add(this.textBoxCards);
            this.groupBoxProbeMux.Controls.Add(this.labelCards);
            this.groupBoxProbeMux.Controls.Add(this.textBoxPort);
            this.groupBoxProbeMux.Controls.Add(this.labelPort);
            this.groupBoxProbeMux.Controls.Add(this.textBoxProbeIP);
            this.groupBoxProbeMux.Controls.Add(this.labelProbeIPAddress);
            this.groupBoxProbeMux.Location = new System.Drawing.Point(200, 156);
            this.groupBoxProbeMux.Name = "groupBoxProbeMux";
            this.groupBoxProbeMux.Size = new System.Drawing.Size(182, 167);
            this.groupBoxProbeMux.TabIndex = 3;
            this.groupBoxProbeMux.TabStop = false;
            this.groupBoxProbeMux.Text = "Probe Multiplexer Settings";
            // 
            // textBoxCards
            // 
            this.textBoxCards.Location = new System.Drawing.Point(70, 92);
            this.textBoxCards.Name = "textBoxCards";
            this.textBoxCards.Size = new System.Drawing.Size(55, 20);
            this.textBoxCards.TabIndex = 10;
            // 
            // labelCards
            // 
            this.labelCards.AutoSize = true;
            this.labelCards.Location = new System.Drawing.Point(30, 95);
            this.labelCards.Name = "labelCards";
            this.labelCards.Size = new System.Drawing.Size(34, 13);
            this.labelCards.TabIndex = 9;
            this.labelCards.Text = "Cards";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(70, 66);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(55, 20);
            this.textBoxPort.TabIndex = 8;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(38, 70);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(26, 13);
            this.labelPort.TabIndex = 7;
            this.labelPort.Text = "Port";
            // 
            // textBoxProbeIP
            // 
            this.textBoxProbeIP.Location = new System.Drawing.Point(70, 40);
            this.textBoxProbeIP.Name = "textBoxProbeIP";
            this.textBoxProbeIP.Size = new System.Drawing.Size(106, 20);
            this.textBoxProbeIP.TabIndex = 2;
            // 
            // labelProbeIPAddress
            // 
            this.labelProbeIPAddress.AutoSize = true;
            this.labelProbeIPAddress.Location = new System.Drawing.Point(6, 43);
            this.labelProbeIPAddress.Name = "labelProbeIPAddress";
            this.labelProbeIPAddress.Size = new System.Drawing.Size(58, 13);
            this.labelProbeIPAddress.TabIndex = 1;
            this.labelProbeIPAddress.Text = "IP Address";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(307, 349);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(226, 349);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 5;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // groupBoxRelayBoard
            // 
            this.groupBoxRelayBoard.Controls.Add(this.labelRelayBoardPort);
            this.groupBoxRelayBoard.Controls.Add(this.comboBoxRelayBoardPort);
            this.groupBoxRelayBoard.Location = new System.Drawing.Point(200, 12);
            this.groupBoxRelayBoard.Name = "groupBoxRelayBoard";
            this.groupBoxRelayBoard.Size = new System.Drawing.Size(182, 66);
            this.groupBoxRelayBoard.TabIndex = 6;
            this.groupBoxRelayBoard.TabStop = false;
            this.groupBoxRelayBoard.Text = "Relay Board Settings";
            // 
            // labelRelayBoardPort
            // 
            this.labelRelayBoardPort.AutoSize = true;
            this.labelRelayBoardPort.Location = new System.Drawing.Point(11, 32);
            this.labelRelayBoardPort.Name = "labelRelayBoardPort";
            this.labelRelayBoardPort.Size = new System.Drawing.Size(53, 13);
            this.labelRelayBoardPort.TabIndex = 1;
            this.labelRelayBoardPort.Text = "COM Port";
            // 
            // comboBoxRelayBoardPort
            // 
            this.comboBoxRelayBoardPort.FormattingEnabled = true;
            this.comboBoxRelayBoardPort.Location = new System.Drawing.Point(70, 29);
            this.comboBoxRelayBoardPort.Name = "comboBoxRelayBoardPort";
            this.comboBoxRelayBoardPort.Size = new System.Drawing.Size(86, 21);
            this.comboBoxRelayBoardPort.TabIndex = 0;
            // 
            // PortConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 384);
            this.Controls.Add(this.groupBoxRelayBoard);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBoxProbeMux);
            this.Controls.Add(this.groupBoxChamber);
            this.Controls.Add(this.groupBoxReference);
            this.Controls.Add(this.groupBoxRootDirectory);
            this.Name = "PortConfigurationForm";
            this.Text = "Port Configuration Utility";
            this.Load += new System.EventHandler(this.PortConfigurationForm_Load);
            this.groupBoxRootDirectory.ResumeLayout(false);
            this.groupBoxRootDirectory.PerformLayout();
            this.groupBoxReference.ResumeLayout(false);
            this.groupBoxReference.PerformLayout();
            this.groupBoxChamber.ResumeLayout(false);
            this.groupBoxChamber.PerformLayout();
            this.groupBoxProbeMux.ResumeLayout(false);
            this.groupBoxProbeMux.PerformLayout();
            this.groupBoxRelayBoard.ResumeLayout(false);
            this.groupBoxRelayBoard.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxRootDirectory;
        private System.Windows.Forms.TextBox textBoxRootDirectory;
        private System.Windows.Forms.GroupBox groupBoxReference;
        private System.Windows.Forms.ComboBox comboBoxReferencePort;
        private System.Windows.Forms.Label labelReferencePort;
        private System.Windows.Forms.TextBox textBoxRefA4;
        private System.Windows.Forms.TextBox textBoxRefA3;
        private System.Windows.Forms.TextBox textBoxRefA2;
        private System.Windows.Forms.TextBox textBoxRefA1;
        private System.Windows.Forms.TextBox textBoxRefA0;
        private System.Windows.Forms.Label labelCoefficients;
        private System.Windows.Forms.TextBox textBoxRefA5;
        private System.Windows.Forms.Label labelRefA0;
        private System.Windows.Forms.Label labelRefA5;
        private System.Windows.Forms.Label labelRefA4;
        private System.Windows.Forms.Label labelRefA3;
        private System.Windows.Forms.Label labelRefA2;
        private System.Windows.Forms.Label labelRefA1;
        private System.Windows.Forms.Button buttonRootDirectory;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogRoot;
        private System.Windows.Forms.GroupBox groupBoxChamber;
        private System.Windows.Forms.Label labelChamberPort;
        private System.Windows.Forms.ComboBox comboBoxChamberPort;
        private System.Windows.Forms.GroupBox groupBoxProbeMux;
        private System.Windows.Forms.TextBox textBoxProbeIP;
        private System.Windows.Forms.Label labelProbeIPAddress;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.TextBox textBoxCards;
        private System.Windows.Forms.Label labelCards;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.GroupBox groupBoxRelayBoard;
        private System.Windows.Forms.Label labelRelayBoardPort;
        private System.Windows.Forms.ComboBox comboBoxRelayBoardPort;
    }
}