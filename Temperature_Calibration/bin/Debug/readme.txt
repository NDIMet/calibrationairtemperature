﻿Change log

1.3.0.0
William Jones
-Modified the shutdown thread to shutdown the chamber only after the chamber reachs 24C or 3600 sec.
-Added a double check to make sure the chamber turns off.
-Added automaticly moving cal files into the cal file server location.