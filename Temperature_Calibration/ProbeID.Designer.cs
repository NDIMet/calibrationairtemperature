﻿namespace Temperature_Calibration
{
    partial class ProbeID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxFirstProbeID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPanels = new System.Windows.Forms.Label();
            this.textBoxPanels = new System.Windows.Forms.TextBox();
            this.checkBoxAddP = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(100, 147);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(12, 147);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxFirstProbeID
            // 
            this.textBoxFirstProbeID.Location = new System.Drawing.Point(12, 46);
            this.textBoxFirstProbeID.Name = "textBoxFirstProbeID";
            this.textBoxFirstProbeID.Size = new System.Drawing.Size(75, 20);
            this.textBoxFirstProbeID.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "First Probe ID";
            // 
            // labelPanels
            // 
            this.labelPanels.AutoSize = true;
            this.labelPanels.Location = new System.Drawing.Point(93, 75);
            this.labelPanels.Name = "labelPanels";
            this.labelPanels.Size = new System.Drawing.Size(71, 13);
            this.labelPanels.TabIndex = 6;
            this.labelPanels.Text = "No. of Panels";
            // 
            // textBoxPanels
            // 
            this.textBoxPanels.Location = new System.Drawing.Point(12, 72);
            this.textBoxPanels.Name = "textBoxPanels";
            this.textBoxPanels.Size = new System.Drawing.Size(75, 20);
            this.textBoxPanels.TabIndex = 5;
            // 
            // checkBoxAddP
            // 
            this.checkBoxAddP.AutoSize = true;
            this.checkBoxAddP.Location = new System.Drawing.Point(12, 111);
            this.checkBoxAddP.Name = "checkBoxAddP";
            this.checkBoxAddP.Size = new System.Drawing.Size(85, 17);
            this.checkBoxAddP.TabIndex = 7;
            this.checkBoxAddP.Text = "Add \'P\' to ID";
            this.checkBoxAddP.UseVisualStyleBackColor = true;
            // 
            // ProbeID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(192, 182);
            this.Controls.Add(this.checkBoxAddP);
            this.Controls.Add(this.labelPanels);
            this.Controls.Add(this.textBoxPanels);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxFirstProbeID);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Name = "ProbeID";
            this.Text = "ProbeID";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProbeID_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxFirstProbeID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPanels;
        private System.Windows.Forms.TextBox textBoxPanels;
        private System.Windows.Forms.CheckBox checkBoxAddP;
    }
}