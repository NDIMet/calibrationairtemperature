﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Temperature_Calibration
{
    static class Program
    {
        public static IPRelay.webRealy chamberRelay;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            chamberRelay = new IPRelay.webRealy("192.168.1.228");

            Application.Run(new Form1());
        }
    }
}
