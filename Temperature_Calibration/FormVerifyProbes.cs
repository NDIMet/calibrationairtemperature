﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Temperature_Calibration
{
    public partial class FormVerifyProbes : Form
    {
        public Form1 dataPasser;

        public bool userAccepted = false;

        public FormVerifyProbes()
        {
            InitializeComponent();
        }

        private void FormVerifyProbes_Load(object sender, EventArgs e)
        {
            //Get the channel data from parent form
            string[,] channelMap = this.dataPasser.channelMap;
            int numberOfPanels = this.dataPasser.numberOfPanels;

            DataTable probeDataTable = new DataTable();
            probeDataTable.Columns.Add("Channel", typeof(string));
            probeDataTable.Columns.Add("Probe ID", typeof(string));
            probeDataTable.Columns.Add("Result", typeof(string));

            dataGridViewProbes.DataSource = probeDataTable;  

            for (int i = 0; i < channelMap.Length / 3 && i < numberOfPanels * 10; i++) 
            {
                probeDataTable.Rows.Add(channelMap[i, 0], channelMap[i, 2], channelMap[i, 1]);
                if (channelMap[i, 1] == "NO")
                {
                    DataGridViewRow badRow = dataGridViewProbes.Rows[i];
                    badRow.DefaultCellStyle.BackColor = System.Drawing.Color.Red;
                }
            }
                      
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.userAccepted = true;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.userAccepted = false;
            this.Close();
        }

        private void FormVerifyProbes_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.dataPasser.childFormClose.Set();
        }


    }
}
